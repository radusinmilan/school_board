SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for board
-- ----------------------------
DROP TABLE IF EXISTS `board`;
CREATE TABLE `board`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of board
-- ----------------------------
INSERT INTO `board` VALUES (1, 'CSM');
INSERT INTO `board` VALUES (2, 'CSMB ');

-- ----------------------------
-- Table structure for grades
-- ----------------------------
DROP TABLE IF EXISTS `grades`;
CREATE TABLE `grades`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `value` int(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_grades_student_student_id_id`(`student_id`) USING BTREE,
  CONSTRAINT `fk_grades_student_student_id_id` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of grades
-- ----------------------------
INSERT INTO `grades` VALUES (1, 1, 8);
INSERT INTO `grades` VALUES (2, 2, 7);
INSERT INTO `grades` VALUES (3, 3, 9);
INSERT INTO `grades` VALUES (4, 1, 5);
INSERT INTO `grades` VALUES (5, 1, 6);
INSERT INTO `grades` VALUES (6, 1, 7);
INSERT INTO `grades` VALUES (7, 2, 6);
INSERT INTO `grades` VALUES (8, 2, 8);
INSERT INTO `grades` VALUES (9, 2, 5);
INSERT INTO `grades` VALUES (10, 3, 8);
INSERT INTO `grades` VALUES (11, 3, 10);

-- ----------------------------
-- Table structure for students
-- ----------------------------
DROP TABLE IF EXISTS `students`;
CREATE TABLE `students`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `board_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of students
-- ----------------------------
INSERT INTO `students` VALUES (1, 'Milan Nenadovic', 1);
INSERT INTO `students` VALUES (2, 'Milan Radusin', 2);
INSERT INTO `students` VALUES (3, 'Nemanja Jovanovic', 1);

SET FOREIGN_KEY_CHECKS = 1;
