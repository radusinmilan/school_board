<?php

namespace MR;

/**
 * Database config
 */
trait Config
{
    /**
     * MySql DataBase parameters
     * Input your values
     */
    protected static $db_host = 'localhost';
    protected static $db_name = 'school';
    protected static $db_user = 'root';
    protected static $db_pass = 'root';
}