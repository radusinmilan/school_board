<?php

namespace MR\models\repositories;

use MR\Database;

/**
 * Student Repository class
 */
class StudentRepository extends Database
{
    /**
     * Get Student By id
     * @param $id
     * @return array|false
     */
    public static function getById($id)
    {
        $query = "SELECT * FROM students WHERE id=$id";

        return self::_select($query);
    }

    /**
     * Get student grades
     * @param $id
     * @return array
     */
    public static function getStudentGrades($id)
    {
        $query = "SELECT * FROM grades WHERE student_id=$id";
        $result = self::_select($query);
        if($result !== false) {
            return $result;
        }

        return [];
    }
}