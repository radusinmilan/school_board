<?php

namespace MR\models;

use MR\models\repositories\StudentRepository;

/**
 * Student Model
 */
class Student
{
    public $id;
    public $name;
    public $board_id;
    public $grades;
    public $average;
    public $finalResult;

    public function __construct($id = false, $name = false, $board_id = false)
    {
        $this->id = $id;
        $this->name = $name;
        $this->board_id = $board_id;
        $this->grades = $this->getGrades();
    }

    /**
     * Get Student by Id
     * @param $id
     * @return false|Student
     */
    public static function getById($id)
    {
        $result = StudentRepository::getById($id);

        if($result !== false) {
            return new Student($result[0]['id'], $result[0]['name'], $result[0]['board_id']);
        }

        return false;
    }

    /**
     * Main function for get Student info
     * @param $id
     * @return string
     */
    public static function getInfo($id)
    {
        $student = self::getById($id);
        if($student !== false){
            return Board::getStudentInfo($student);
        }

        return 'Something went wrong.';
    }

    /**
     * Get Student grades
     * @return array
     */
    public function getGrades()
    {
        return StudentRepository::getStudentGrades($this->id);
    }

    /**
     * Calculate average of student grades
     *
     */
    public function calculateAverage()
    {
        if(empty($this->grades)){
            $this->average = 0;
        } else {
            $total = 0;
            foreach ($this->grades as $g) {
                $total += $g['value'];
            }
            $this->average = $total/count($this->grades);
        }
    }

    /**
     * Delete the lowest grade from student if student has more than two grades
     */
    public function discardLowestGrade()
    {
        if(count($this->grades) > 2)
        {
            $index = 0;
            $size = count($this->grades);
            $min = 11;

            // Find the lowest grade index
            for($i = 0; $i < $size; $i++) {
                if($this->grades[$i]['value'] < $min) {
                    $min = $this->grades[$i]['value'];
                    $index = $i;
                }
            }
            //delete lowest grade from array
            unset($this->grades[$index]);
        }
    }

    /**
     * Find the max grade from student
     */
    public function getMaxGrade()
    {
        $size = count($this->grades);
        $max = 0;

        for($i = 0; $i < $size; $i++) {
            if($this->grades[$i]['value'] > $max) {
                $max = $this->grades[$i]['value'];
            }
        }

        return $max;
    }

    /**
     * set final result
     */
    public function setFinalResult()
    {
        $this->finalResult = $this->getMaxGrade() > 8 ? 'pass' : 'fail';
    }

    public function getJson()
    {
        $array = (array)$this;
        unset($array['board_id']);
        if($array['average'] > 0){
            foreach ($array['grades'] as &$grade) {
                unset($grade['id']);
                unset($grade['student_id']);
            }
        } else {
            $array['grades'] = "Student doesn't have grades";
        }

        return json_encode($array);
    }

    /**
     * return xml presentation of student
     */
    public function getXml()
    {
        $array = (array)$this;
        unset($array['board_id']);
        if($array['average'] > 0){
            foreach ($array['grades'] as &$grade) {
                unset($grade['id']);
                unset($grade['student_id']);
            }
        } else {
            $array['grades'] = "Student doesn't have grades";
        }

        return  $this->arrayToXml($array);
    }

    /**
     * Convert array to xml string
     */
    public  function arrayToXml($array, $rootElement = null, $xml = null)
    {
        $_xml = $xml;
        if ($_xml === null) {
            $_xml = new \SimpleXMLElement($rootElement !== null ? $rootElement : '<student/>');
        }

        foreach ($array as $k => $v) {
            if (is_array($v)) {
                $this->arrayToXml($v, $k, $_xml->addChild($k));
            }else {
                $_xml->addChild($k, $v);
            }
        }

        return $_xml->asXML();
    }
}
