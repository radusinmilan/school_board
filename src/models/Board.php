<?php

namespace MR\models;

/**
 * Board Model
 */
class Board
{
    private $id;
    private $name;

    /**
     * Get Student info by BoardId
     * @param Student $student
     * @return bool|string
     */
    public static function getStudentInfo(Student $student)
    {
        switch ($student->board_id) {
            case 1:
                return self::csm($student);
            case 2:
                return self::csmb($student);
            default:
                return 'Unknown school board.';
        }
    }

    /**
     * Return info about student grades
     * @param Student $student
     * @return false|string
     */
    private function csm(Student $student)
    {
        $student->calculateAverage();

        $student->finalResult = $student->average >= 7 ? 'pass' : 'fail';
        return $student->getJson();
    }

    /**
     * Return info about student grades
     * @param Student $student
     * @return bool|string
     */
    private function csmb(Student $student)
    {
        $student->discardLowestGrade();
        $student->calculateAverage();
        $student->setFinalResult();
        return $student->getXml();
    }
}