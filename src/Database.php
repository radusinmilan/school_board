<?php

namespace MR;

/**
 * Milan Radusin 02. 04. 2022
 * Database class for interact with database
 */
class Database
{
    use Config;

    /**
     * @return false|\PDO
     */
    private static function _connect()
    {
        try {
            $conn = new \PDO(
                'mysql:host='.self::$db_host.
                ';dbname='.self::$db_name.
                ';charset=utf8',
                self::$db_user,
                self::$db_pass
            );
            $conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        } catch (\PDOException $e) {
            $conn = false;
        }

        return $conn;
    }

    /**
     * Select command for execute a query
     * @param $q
     * @return array|false
     */
    public static function _select($q)
    {
        try {
            $c = self::_connect();
            $stmt = $c->prepare($q);
            $stmt->execute();
            $stmt->setFetchMode(\PDO::FETCH_ASSOC);
            $result = $stmt->fetchAll();
            $result = empty(array_filter($result)) ? false : $result;
        } catch (\PDOException $e) {
            $result = false;
        }
        $c = null;
        return $result;
    }
}