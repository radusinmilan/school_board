## Instalation process

+ Clone repository
```
git clone https://radusinmilan@bitbucket.org/radusinmilan/school_board.git
```

+ Go to project root
```
cd school_board
```

+ Run composer update command
```
composer update
```

+ Import school.sql file from root of project to your database

+ Fill ```/src/Config.php``` with your database credentials
