<?php

use MR\models\Student;

require_once __DIR__ . '/vendor/autoload.php';

// Get id from student param request
$studentId = isset($_GET['student']) ? $_GET['student'] : 0;

echo Student::getInfo($studentId);
